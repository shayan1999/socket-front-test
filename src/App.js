import React from "react";
import {SocketContext, socket} from './context/socket';
import Page from './comps/page1';

function App() {
  return (
    <SocketContext.Provider value={socket}>
      <Page/>
    </SocketContext.Provider>
  );
}

export default App;