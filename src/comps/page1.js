import React, {useState, useEffect, useContext} from "react"
import {SocketContext} from '../context/socket';

function Page1(props){

  const [token, setToken]= useState('');
  const [userId, setUserId]= useState('');
  const [myLogs, setLogs]= useState([]);
  const [vote, setVote]= useState(true);
  const [secondVote, setSecondVote]= useState("");

  const socket = useContext(SocketContext);
  console.log(myLogs);
  useEffect(() => {
    socket.on("join", data => {
      let add= [...myLogs, data];
      setLogs(add);
    });
    
  },[]);
  useEffect(()=>{
    socket.on("getVote", data=>{
      console.log(myLogs);
      let add= [...myLogs, data];
      setLogs(add);
    });
  })

  useEffect(()=>{
    socket.on("finishVote", data=>{
      console.log(data);
      let add= [...myLogs, data];
      setLogs(add);
    })
  })

  useEffect(()=>{
    socket.on("secondVote", data=>{
      console.log(data);
      let add= [...myLogs, data];
      setLogs(add);
    })
  })

    return(
      <div style={{direction: 'rtl'}}>
        <input type="text" id="token" name="token" value={token} onChange={e=> setToken(e.target.value)}/>
        <button onClick={()=>{
          socket.emit("join", {token: token, room_id: '609bbf9ebf7fd74bd05cebff'});
        }} >join</button>
        <br/>
        <br/>
        <input type="text" id="user" name="user" value={userId} onChange={e=> setUserId(e.target.value)}/>
        <button onClick={()=>{
          socket.emit('getVote', {
            room_id: '609bbf9ebf7fd74bd05cebff',
            isVote: vote,
            userId: userId,
            token: token
          });
        }}>
          vote
        </button>
        <input type="checkbox" id="vehicle1" name="vehicle1" checked={vote} onChange={(e)=>setVote(!vote)}/>
        <label for="vehicle1"> I want to vote him</label>
        <br/> <br/>
        <input type="text" id="user2" name="user2" value={secondVote} onChange={e=> setSecondVote(e.target.value)}/>
        <button onClick={()=>{
          console.log(secondVote);
          socket.emit('secondVote', {
            room_id: '609bbf9ebf7fd74bd05cebff',
            userId: secondVote,
            token: token
          });
        }}>
          vote second
        </button>
        <br/> <br/>
        {myLogs.map((data, idx)=>{
          return(
            <p style={{color:(idx%2===0)?'red':'green'}}>
              **{data.message}--{JSON.stringify(data.data)}
            </p>
          )
        })}
    </div>
    )
}

export default Page1;